{{% callout note %}}
This model requires the external file [`M6694_model_domain.tiff`](/media/model/m6694/M6694_model_domain.tiff), which needs to be in the same directory as {{< model_quick_access "media/model/m6694/M6694_model_mIFFL.xml" >}}.
{{% /callout %}}

![](M6694_model_graph.svg "Model Graph")