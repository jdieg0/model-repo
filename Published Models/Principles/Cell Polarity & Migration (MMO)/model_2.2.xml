<MorpheusModel version="4">
    <Description>
        <Title>Example-CellPolarity</Title>
        <Details>Full title:    Mixed-mode oscillations in a Rac/Rho/Paxillin subsystem   
Date: 	06.01.2023
Authors: 	L. Plazen
Software: 	Morpheus (open-source), download from https://morpheus.gitlab.io	
Reference:    This model is described in Polarity and mixed-mode oscillations may underlie different patterns of cellular migration, L. Plazen, J. Al Rahbani, C.M. Brown, A. Khadra
Currently under review
</Details>
    </Description>
    <Global>
        <Field symbol="U" value="0"/>
        <Function symbol="x">
            <Expression>discr*lattice.x</Expression>
        </Function>
        <Function symbol="y">
            <Expression>discr*lattice.y</Expression>
        </Function>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="lattice" value="250, 250, 0"/>
            <NodeLength symbol="discr" value="0.1"/>
            <BoundaryConditions>
                <Condition boundary="x" type="periodic"/>
                <Condition boundary="y" type="periodic"/>
            </BoundaryConditions>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
        <MembraneLattice>
            <Resolution symbol="memsize" value="200"/>
            <SpaceSymbol symbol="m"/>
        </MembraneLattice>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="5000"/>
        <SaveInterval value="0"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="19"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="cells">
            <Protrusion field="U" maximum="1000" strength="2"/>
            <VolumeConstraint target="1200" strength="0.0004"/>
            <SurfaceConstraint target="200" mode="surface" strength="0.002"/>
            <ConnectivityConstraint/>
            <MembraneProperty symbol="R" value="if((m.phi&lt;=0.1 and m.phi>0) or (m.phi >0.3 and m.phi&lt;0.4) or (m.phi >0.5 and m.phi&lt;0.65) or (m.phi >0.7 and m.phi&lt;0.72) or (m.phi >2 and m.phi&lt;2.4) or (m.phi >3.5 and m.phi&lt;3.7) or (m.phi >4 and m.phi&lt;4.2) or (m.phi >5.1 and m.phi&lt;5.7) or (m.phi >6 and m.phi&lt;6.2) or (m.phi >7 and m.phi&lt;7.2)  , 0.3, 0.05)" name="R">
                <Diffusion rate="0.0025"/>
            </MembraneProperty>
            <MembraneProperty symbol="RI" value="if((m.phi&lt;=0.1 and m.phi>0) or (m.phi >0.3 and m.phi&lt;0.4) or (m.phi >0.5 and m.phi&lt;0.65) or (m.phi >0.7 and m.phi&lt;0.72) or (m.phi >2 and m.phi&lt;2.4) or (m.phi >3.5 and m.phi&lt;3.7) or (m.phi >4 and m.phi&lt;4.2) or (m.phi >5.1 and m.phi&lt;5.7) or (m.phi >6 and m.phi&lt;6.2) or (m.phi >7 and m.phi&lt;7.2), 0.4669, 0.8385)" name="RI">
                <Diffusion rate="0.43"/>
            </MembraneProperty>
            <MembraneProperty symbol="B" value="if((m.phi&lt;=0.1 and m.phi>0) or (m.phi >0.3 and m.phi&lt;0.4) or (m.phi >0.5 and m.phi&lt;0.65) or (m.phi >0.7 and m.phi&lt;0.72) or (m.phi >2 and m.phi&lt;2.4) or (m.phi >3.5 and m.phi&lt;3.7) or (m.phi >4 and m.phi&lt;4.2) or (m.phi >5.1 and m.phi&lt;5.7) or (m.phi >6 and m.phi&lt;6.2) or (m.phi >7 and m.phi&lt;7.2)  , 0.5, 2)" name="B">
                <Diffusion rate="0"/>
            </MembraneProperty>
            <MembraneProperty symbol="kb" value="0.05" name="kb">
                <Diffusion rate="0"/>
            </MembraneProperty>
            <System solver="stochastic" time-step="0.1" name="4DPDE">
                <Constant symbol="a" value="0.6281" />
                <Constant symbol="d" value="2.8773" />
                <Constant symbol="c" value="11.5" />
                <Constant symbol="Irho" value="0.016" name="Rho activation rate"/>
                <Constant symbol="IR" value="0.0035" name="Rac activation rate"/>
                <Constant symbol="Ik" value="0.009" name="Paxillin basal activation rate"/>
                <Constant symbol="LR" value="0.34" name="Half max value in the Hill function of Rac"/>
                <Constant symbol="Lrho" value="0.34" name="Half max value in the Hill function of Rho"/>
                <Constant symbol="LK" value="5.77" name="Half max value in the Hill function of paxillin"/>
                <Constant symbol="gamma" value="0.3" />
                <Constant symbol="epsilon" value="0.01" />
                <Constant symbol="epsilon_kb" value="0.00001" />
                <Constant symbol="gammaR" value="8.69565" name="Parameter for the nullcline"/>
                <Constant symbol="alphaR" value="15" />
                <Constant symbol="alphaP" value="2.7" name="Linearization of Paxillin"/>
                <Constant symbol="n" value="4" name="Non linearity level, Hill coefficient"/>
                <Constant symbol="delta_P" value="0.00041" name="Paxillin inactivation rate for Paxillin"/>
                <Constant symbol="delta_rho" value="0.016" name="Rho inactivation rate"/>
                <Constant symbol="delta_rac" value="0.025" name="Rac inactivation rate"/>
                <DiffEqn symbol-ref="R" name="PDE for active Rac">
                    <Expression>(IR+Iks())* (Lrho^n/ ( Lrho^n + Rho()^n) )*(RI) - delta_rac*R </Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="RI" name="PDE for inactive Rac">
                    <Expression>-(IR+Iks())* (Lrho^n/ ( Lrho^n + Rho()^n) )*(RI) + delta_rac*R </Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="B" name="PDE for B">
                    <Expression>dBdt</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="kb" name="PDE for kb">
                    <Expression>epsilon_kb*(0.15-R)</Expression>
                </DiffEqn>
                <Function symbol="dBdt" name="Derivative for B">
                    <Expression>epsilon*(1-kb*(B-10) -gammaR*R + 0.00001/(B+0.00001))</Expression>
                </Function>
                <Function symbol="K" name="Expression of K">
                    <Expression>alphaR*R /(1 + alphaR * R + (a*d)/(1 + d + 3/(1+1*R)))</Expression>
                </Function>
                <Function symbol="Rho" name="Expression of Rho in the QSSA">
                    <Expression>( Irho*LR^n)/( Irho*LR^n + delta_rho*( LR^n + (R + gamma*K())^n )   )</Expression>
                </Function>
                <Function symbol="Iks" name="Paxillin activation rate">
                    <Expression>Ik*(1 - 1/(1 + d +  a*d*c*P + 0.5 ))</Expression>
                </Function>
                <Function symbol="P" name="Active Paxillin">
                    <Expression>B*(K()^n/(LK^n+K()^n))/( alphaP*B*(K()^n/(LK^n+K()^n)) + delta_P) </Expression>
                </Function>
            </System>
            <Mapper>
                <Input value="R-0.15"/>
                <Output symbol-ref="U"/>
            </Mapper>
        </CellType>
    </CellTypes>
    <CPM>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="0.5"/>
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="0.12" yield="0.1"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
        </ShapeSurface>
        <Interaction/>
    </CPM>
    <CellPopulations>
        <Population size="0" type="cells">
            <InitCellObjects mode="distance">
                <Arrangement repetitions="1,1,1" displacements="0,0,0">
                    <Sphere radius="20" center="lattice.x/2,lattice.x/2,0"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="1000">
            <Terminal name="png"/>
            <Plot>
                <Field symbol-ref="cell.id">
                    <ColorMap>
                        <Color value="1" color="white"/>
                        <Color value="0" color="black"/>
                    </ColorMap>
                </Field>
            </Plot>
        </Gnuplotter>
        <CellTracker format="ISBI 2012 (XML)" time-step="5"/>
        <DisplacementTracker time-step="1" celltype="cells"/>
    </Analysis>
</MorpheusModel>
