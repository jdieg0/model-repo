<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Spatially distributed Model I (Related to the Model 3 in Holmes et al (2017)

R= Rac, P = Rho, E = ECM

Rac-Rho equations
d R/dt= {b_R }/{1+\rho_a^3}R_I - \delta R +D_R Laplacian( R),
d \rho/dt ={b_\rho }{1+R_a^3}\rho_I - \delta \rho +D_\rho Laplacian( Rho)

ECM equation

dE/dt= epsilon*((K+GammaR*f_R1())-E*(kP+GammaP*f_P1()))
where the f's are Hill functions of Rac or Rho.</Details>
        <Title>Model1RacRhoECMPDEsIn1D</Title>
    </Description>
    <Space>
        <Lattice class="linear">
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <Size symbol="size" value="30, 0, 0"/>
            <NodeLength symbol="dx" value="0.1"/>
            <BoundaryConditions>
                <Condition boundary="x" type="noflux"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <Global>
        <Constant symbol="bR" value="5" name="Rac activation rate"/>
        <Constant symbol="delta" value="1.0" name="Rac decay rate"/>
        <Constant symbol="kE" value="2" name="Rho basal activation rate"/>
        <Constant symbol="GammaE" value="4" name="Rho activation due to ECM feedback"/>
        <Constant symbol="GammaP" value="10" name="Rho feedback to ECM reduction"/>
        <Constant symbol="GammaR" value="5.0" name="Rac-dependent ECM rate of increase"/>
        <Constant symbol="R0" value="1.0" name="Rac level for half-max ECM activation"/>
        <Constant symbol="P0" value="2.4" name="Rho level for half-max ECM inhibition"/>
        <Constant symbol="epsilon" value="0.001" name="1/( ECM timescale)"/>
        <Constant symbol="K" value="0.1" name="ECM basal rate of increase"/>
        <Constant symbol="kP" value="0.45" name="Rho-induced ECM downregulation"/>
        <Constant symbol="E0" value="1.5" name="ECM level for half-max Rho activation"/>
        <Constant symbol="n" value="3" name="Hill coefficient"/>
        <Function symbol="x">
            <Expression>dx*space.x</Expression>
        </Function>
        <Field symbol="P" value="0.0" name="Rho">
            <Diffusion rate="0.1"/>
        </Field>
        <Field symbol="R" value="if(x&lt;=0.3, 4, 0)" name="Rac">
            <Diffusion rate="0.1"/>
        </Field>
        <Field symbol="E" value="0&#xa;" name="ECM contact">
            <Diffusion rate="0"/>
        </Field>
        <Field symbol="RI" value="1.5" name="Inactive Rac">
            <Diffusion rate="1"/>
        </Field>
        <Field symbol="PI" value="1.5" name="Inactive Rho">
            <Diffusion rate="1"/>
        </Field>
        <Field symbol="plotR" value="0.0" name="Rac for plotting"/>
        <System solver="adaptive45" time-step="0.1">
            <DiffEqn symbol-ref="R" name="PDE for active Rac">
                <Expression>(bR/(1+P^n))*RI-1*delta*R</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="P" name="PDE for active Rho">
                <Expression>(kE+GammaE*f_E1())*PI/(1+R^n)-1*P</Expression>
            </DiffEqn>
            <Function symbol="f_E1" name="function for ECM-dependent rate of Rho activation ">
                <Expression>E^3/(E0^3+E^3)</Expression>
            </Function>
            <DiffEqn symbol-ref="E" name="PDE for ECM variable">
                <Expression>epsilon*((K+GammaR*f_R1())-E*(kP+GammaP*f_P1()))</Expression>
            </DiffEqn>
            <Function symbol="f_R1" name="Formula for Rac-dependent ECM activation">
                <Expression>R^n/(R0^n+R^n)</Expression>
            </Function>
            <Function symbol="f_P1" name="formula for Rho-dependent ECM supression">
                <Expression>P^n/(P0^n+P^n)</Expression>
            </Function>
            <DiffEqn symbol-ref="RI" name="PDE for inactive Rac">
                <Expression>(-bR/(1+P^n))*RI+1*delta*R</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="PI" name="PDE for inactive Rho">
                <Expression>-(kE+GammaE*f_E1())*PI/(1+R^n)+1*P</Expression>
            </DiffEqn>
            <Rule symbol-ref="ECM" name="ECM">
                <Expression>E</Expression>
            </Rule>
            <Rule symbol-ref="plotR">
                <Expression>min(R,2)</Expression>
            </Rule>
        </System>
        <Field symbol="ECM" value="0"/>
    </Global>
    <Analysis>
        <Logger time-step="1" name="chemical profiles">
            <Input>
                <Symbol symbol-ref="R"/>
                <Symbol symbol-ref="P"/>
                <Symbol symbol-ref="E"/>
                <Symbol symbol-ref="PI"/>
                <Symbol symbol-ref="RI"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="-1">
                    <Style point-size="4.0" style="points" decorate="false"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="time"/>
                    </Y-axis>
                    <Color-bar minimum="0.0" maximum="2">
                        <Symbol symbol-ref="plotR"/>
                    </Color-bar>
                </Plot>
                <Plot time-step="10">
                    <Style style="lines" line-width="2.0"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="x"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="7">
                        <Symbol symbol-ref="P"/>
                        <Symbol symbol-ref="ECM"/>
                        <Symbol symbol-ref="R"/>
                    </Y-axis>
                    <Range>
                        <Time mode="current" history="1.0"/>
                    </Range>
                </Plot>
            </Plots>
        </Logger>
    </Analysis>
</MorpheusModel>
