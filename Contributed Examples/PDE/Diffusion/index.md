---
MorpheusModelID: M2017

title: Diffusion

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- 1D
- 2D
- Boundary Condition
- BoundaryConditions
- Diffusion
- Partial Differential Equation
- PDE

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Simulations of a diffusing field

## Introduction

Here, we build up a simulation of diffusion over three successive files by:

1. Starting with 1D and a short time scale in {{< model_quick_access "media/model/m2017/Diffusion1a_main.xml" >}}
,
2. simulating for longer times in {{< model_quick_access "media/model/m2017/Diffusion1b.xml" >}}
 and
3. extending to 2D in {{< model_quick_access "media/model/m2017/Diffusion2a.xml" >}}
.

## Description

Chemical diffusion and decay are easily simulated in Morpheus in 1D or 2D. We demonstrate the numerical simulation of the PDE and boundary conditions:

$$\begin{align}
\frac{\partial c}{\partial t} = D \frac{\partial^2 c}{\partial x^2} - kc \quad c(0) = 0, c(L) = 5
\end{align}$$

## Results

<figure>
    ![](DiffusionMorpheusSimBU.png)
    <figcaption >
        Building up a simulation of diffusion in Morpheus: (a) Starting with 1D and short time scale ({{< model_quick_access "media/model/m2017/Diffusion1a_main.xml" >}}), (b) for longer times ({{< model_quick_access "media/model/m2017/Diffusion1b.xml" >}}), and (c) extension to 2D ({{< model_quick_access "media/model/m2017/Diffusion2a.xml" >}}).
    </figcaption>
</figure>
