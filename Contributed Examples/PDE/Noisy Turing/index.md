---
MorpheusModelID: M5483

authors: [S. Miyazawa, M. Okamoto and S. Kondo]
contributors: [J. Starruß, L. Brusch]

title: "Turing Patterns with Noise"

# Reference details
publication:
  doi: "10.1038/ncomms1071"
  title: "Blending of animal colour patterns by hybridization"
  journal: "Nature Communications"
  volume: 1
  page: "66"
  year: 2010
  original_model: false
  
tags:
- 2D
- Euler–Maruyama Method
- Gaussian Noise
- Noise
- Noise-induced Pattern
- ODE
- Ordinary Differential Equation
- PDE
- Partial Differential Equation
- Spatial Model
- Stochastic Model
- Turing
- Turing Pattern
- White Gaussian Noise

categories:
- DOI:10.1038/ncomms1071
---

> Pattern formation is robust to noise.

## Introduction

Morpheus can also simulate ODE and PDE models with noise. One example (in the GUI menu `Examples` → `ODE` → `LateralSignaling.xml`) with coupled noisy ODE systems is explained in [M0004](/model/m0004). To illustrate pattern formation in a noisy PDE system, we here revisit the example [M0014](/model/m0014) (in the GUI menu `Examples` → `PDE` → `TuringPatterns.xml`).

## Model Description

We have extended the deterministic PDE model [M0014](https://identifiers.org/morpheus/M0014) which was originally published in [Miyazawa _et al._](#reference) by adding Gaussian white (in space and time) noise to both PDEs. The solver was switched to Euler-Maruyama as needed for stochastic ODE or PDE models. As in the original publication, the pattern's dependency on parameter values is scanned in a single simulation run by $`x`$- and $`y`$-dependent parameter values $`A(y)`$ and $`C(x)`$ across the 2D lattice. 

## Results

### Without noise

Setting the parameter `noise` $`= 0`$, the initial heterogeneities lead to some frustrated arrangements where a stripe extends into the domain of dot-patterns or dots are found in the central labyrinth domain.

![](plot_without_noise.png "Simulation result for field `u` at `time` $`=100`$ without noise, white denotes zero and black above 1.")

These mixed stripe-dot arrangements are expected to resolve very slowly (beyond the runtime of this movie).

![](movie_without_noise.mp4)

### With noise

When adding little noise, then the above frustrated arrangements resolve faster (not shown).

With high noise level (`noise` $`= 0.2`$), we observe that the same patterns emerge robustly in the same domains as without noise.

![](movie_with_noise.mp4)

Moreover, continuously perturbing the fluxes keeps the state away from the homogeneous solution also in outside the Turing instability domain (towards the homogeneous white and black corners). There, the state relaxes back to the homogeneous state and gets pushed away again in a manner that the slowest-decaying spatial models dominate and are visible as apparent continuation of the Turing patterns (hexagonal array of dots) beyond the Turing instability border. These are known as noise-induced patterns.

![](plot_with_noise.png "Simulation result for field `u` at `time` $`= 100`$ with `noise` $` = 0.2`$, again white denotes zero and black above 1.")
