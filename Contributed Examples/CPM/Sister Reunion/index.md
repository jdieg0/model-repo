---
MorpheusModelID: M5493

contributors: [L. Brusch]

title: Sister Reunion
date: "2020-06-05T10:42:00+02:00"
lastmod: "2020-11-10T14:49:00+01:00"
---

>Track ```MotherId``` (```cell.id``` of mother) and compare it between neighboring cells

## Introduction

This example model demonstrates the tracking of the ```cell.id``` of any cell's mother and to compare these ```MotherId```s between neighboring cells upon contact to trigger special behaviors if sister cells meet in a SisterReunion. Such special cell-cell contacts of sister cells are flagged by the cell property ```SisterReunion``` $ = 1$, otherwise $0$.

## Description

For each cell division, a trigger copies the ```cell.id``` of the mother cell to the property ```MotherId``` of each daughter cell. In the included movie, the left panel shows ```cell.id```, the right panel ```MotherId```.
The plugin ```NeighborhoodReporter``` compares ```local.MotherId == MotherId``` where ```local``` accesses the cell's own scope when collecting information from the neighborhood. The test returns a $1$ when sisters are in contact, $0$ otherwise. This is updated every timestep (can be adjusted in ```NeighborhoodReporter```) and stored in the ```Property``` ```SisterReunion``` and used to color cells in the right panel of the movie.

![](SisterReunion.png "Snapshot of moving and dividing cells, daughter cells from the same earlier cell division that later encounter each other again are colored red in the right panel, as indicated by the computed property SisterReunion ($0 = $ green, $1 = $ red)")

![Movie visualising the simulation](SisterReunionMovie.mp4)

A follow-up question may be what happens when multiple cells are in contact of which at most two may be sisters. To control the outcome, you may choose a statistical function in the mapping attribute of the ```NeighborhoodReporter```/```Output```. The statistical function is then applied to all pairwise results of the ```NeighborhoodReporter``` with each neighboring cell. In the attached files, I've used the mapping attribute ```maximum``` to get enough red color into the movie. 
If you choose ```minimum``` then you could flag just those sister reunions that are happening privately. Or you choose ```scaling="length"``` and ```mapping="average"``` to average over the cell membrane fractions (as opposed to each neighbor having the same weight independent of contact length, which you get with ```scaling="cell"```, ```mapping="average"```) in contact with neighboring cells (mind the medium).